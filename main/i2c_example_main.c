// LSM303-I2C Code - Adapted from Expressif I2C example code.

#include <stdio.h>
#include "driver/i2c.h"

/**
 *
 * Pin assignment:
 *
 * - master:
 *    GPIO18 is assigned as the data signal of i2c master port
 *    GPIO19 is assigned as the clock signal of i2c master port
 *
 * Connection:
 *
 * - connect sda/scl of sensor with GPIO18/GPIO19
 * - no need to add external pull-up resistors, driver will enable internal pull-up resistors.
 *
 * Test items:
 *
 * - read the sensor data, if connected.
 */

#define I2C_EXAMPLE_MASTER_SCL_IO          19               /*!< gpio number for I2C master clock */
#define I2C_EXAMPLE_MASTER_SDA_IO          18               /*!< gpio number for I2C master data  */
#define I2C_EXAMPLE_MASTER_NUM             I2C_NUM_0        /*!< I2C port number for master dev */
#define I2C_EXAMPLE_MASTER_TX_BUF_DISABLE  0                /*!< I2C master do not need buffer */
#define I2C_EXAMPLE_MASTER_RX_BUF_DISABLE  0                /*!< I2C master do not need buffer */
#define I2C_EXAMPLE_MASTER_FREQ_HZ         100000            /*!< I2C master clock frequency */

#define WRITE_BIT                          I2C_MASTER_WRITE /*!< I2C master write */
#define READ_BIT                           I2C_MASTER_READ  /*!< I2C master read */
#define ACK_CHECK_EN                       0x1              /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS                      0x0              /*!< I2C master will not check ack from slave */
#define ACK_VAL                            0x0              /*!< I2C ack value */
#define NACK_VAL                           0x1              /*!< I2C nack value */

//https://cdn-shop.adafruit.com/datasheets/LSM303DLHC.PDF

//Sub-address bit to R/W multiple addresses
#define LSM303_RW_MULT (0x80)

//Slave address for accelerometer
#define LSM303_SLAVE_ADDR_ACC (0x19)
//Config register 1A sub-address
#define LSM303_CFG_1A (0x20)
//Config register 4A sub-address
#define LSM303_CFG_4A (0x23)
//Start address of the 6 data bytes
#define LSM303_ACC_OUT_START (0x28)
//Number of data bytes
#define LSM303_ACC_DATA_LEN (6)
//Configure (1A) for highest rate, all axes on.
#define LSM303_CFG_1A_ON (0x97)
//Configure (4A) for high resolution, +-16G full-scale.  Bits 1 and 2 must be 0.
#define LSM303_CFG_4A_FSHR (0x38)

//Slave address for the magnetometer
#define LSM303_SLAVE_ADDR_MAG (0x1E)
//Config reg A sub-address
#define LSM303_CFG_MA (0x00)
//Config reg B sub-address
#define LSM303_CFG_MB (0x01)
//Mode select reg
#define LSM303_CFG_MM (0x02)
//Mag output start sub-address
#define LSM303_MAG_OUT_START (0x03)
//Number of data bytes
#define LSM303_MAG_DATA_LEN (6)
//Temp output start sub-address
#define LSM303_TMP_OUT_START (0x31)
//Number of data bytes (actually 1.5)
#define LSM303_TMP_DATA_LEN (2)
//Configure (A) for the highest rate, temp sensor enabled
#define LSM303_CFG_MA_ON (0x9C)
//Configure (B) for +-1.3 guass (earth is +-0.65 at most)
#define LSM303_CFG_MB_LOW (0x20)
//Configure (mode) for continuous conversion
#define LSM303_CFG_MM_CONT (0x00)

//Temperature sensor LSB per degree
#define TMP_LSB_PER_DEG (8)
//Accelerometer LSB per G (+-16G, hence 5 MSBs control whole number of Gs, hence 11 bits partial number of Gs)
#define ACC_LSB_PER_G (2048)
//Magnetometer LSB per microtesla.  +-130 microtesla = 16 bits, (2^15)/130 = 252 (highest bit is direction)
#define MAG_LSB_PER_UT (252)

typedef unsigned char byte;

SemaphoreHandle_t print_mux = NULL;

static void i2c_example_master_init()
{
  int i2c_master_port = I2C_EXAMPLE_MASTER_NUM;
  i2c_config_t conf;
  conf.mode = I2C_MODE_MASTER;
  conf.sda_io_num = I2C_EXAMPLE_MASTER_SDA_IO;
  conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
  conf.scl_io_num = I2C_EXAMPLE_MASTER_SCL_IO;
  conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
  conf.master.clk_speed = I2C_EXAMPLE_MASTER_FREQ_HZ;
  i2c_param_config(i2c_master_port, &conf);
  i2c_driver_install(i2c_master_port, conf.mode,
		     I2C_EXAMPLE_MASTER_RX_BUF_DISABLE,
		     I2C_EXAMPLE_MASTER_TX_BUF_DISABLE, 0);
}

int i2c_read_check(esp_err_t ret, char *sector){
  if(ret == ESP_ERR_INVALID_ARG){
    printf("Error: Invalid argument in %s read\n", sector);
    return 1;
  }
  else if(ret == ESP_FAIL){
    printf("Error: No ACK in %s read\n", sector);
    return 2;
  }
  else if(ret == ESP_ERR_INVALID_STATE){
    printf("Error: ESP I2C state invalid in %s read\n", sector);
    return 3;
  }
  else if(ret == ESP_ERR_TIMEOUT){
    printf("Error: Operation timed out in %s read\n", sector);
    return 4;
  }
  else if(ret != ESP_OK){
    printf("Error %i in %s read.\n", ret, sector);
    return 5;
  }
  return 0;
}

int i2c_write_check(esp_err_t ret, char *sector){
  if(ret == ESP_ERR_INVALID_ARG){
    printf("Error: Invalid argument in %s write\n", sector);
    return 1;
  }
  else if(ret == ESP_FAIL){
    printf("Error: No ACK in %s write\n", sector);
    return 2;
  }
  else if(ret == ESP_ERR_INVALID_STATE){
    printf("Error: ESP I2C state invalid in %s write\n", sector);
    return 3;
  }
  else if(ret == ESP_ERR_TIMEOUT){
    printf("Error: Operation timed out in %s write\n", sector);
    return 4;
  }
  else if(ret != ESP_OK){
    printf("Error %i in %s write.\n", ret, sector);
    return 5;
  }
  return 0;
}

esp_err_t lsm303_read_acc(byte *acc_data){
  esp_err_t ret;
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, ( LSM303_SLAVE_ADDR_ACC << 1 ) | WRITE_BIT, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_RW_MULT | LSM303_ACC_OUT_START, ACK_CHECK_EN);
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, ( LSM303_SLAVE_ADDR_ACC << 1 ) | READ_BIT, ACK_CHECK_EN);
  i2c_master_read(cmd, acc_data, LSM303_ACC_DATA_LEN-1, ACK_VAL);
  i2c_master_read_byte(cmd, acc_data+LSM303_ACC_DATA_LEN-1, NACK_VAL);
  i2c_master_stop(cmd);
  ret = i2c_master_cmd_begin(I2C_EXAMPLE_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);
  return ret;
}

esp_err_t lsm303_read_mag(byte *mag_data){
  esp_err_t ret;
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, ( LSM303_SLAVE_ADDR_MAG << 1 ) | WRITE_BIT, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_RW_MULT | LSM303_MAG_OUT_START, ACK_CHECK_EN);
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, ( LSM303_SLAVE_ADDR_MAG << 1 ) | READ_BIT, ACK_CHECK_EN);
  i2c_master_read(cmd, mag_data, LSM303_MAG_DATA_LEN-1, ACK_VAL);
  i2c_master_read_byte(cmd, mag_data+LSM303_MAG_DATA_LEN-1, NACK_VAL);
  i2c_master_stop(cmd);
  ret = i2c_master_cmd_begin(I2C_EXAMPLE_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);
  return ret;
}

esp_err_t lsm303_read_tmp(byte *tmp_data){
  esp_err_t ret;
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, ( LSM303_SLAVE_ADDR_MAG << 1 ) | WRITE_BIT, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_RW_MULT | LSM303_TMP_OUT_START, ACK_CHECK_EN);
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, ( LSM303_SLAVE_ADDR_MAG << 1 ) | READ_BIT, ACK_CHECK_EN);
  i2c_master_read(cmd, tmp_data, LSM303_TMP_DATA_LEN-1, ACK_VAL);
  i2c_master_read_byte(cmd, tmp_data+LSM303_TMP_DATA_LEN-1, NACK_VAL);
  i2c_master_stop(cmd);
  ret = i2c_master_cmd_begin(I2C_EXAMPLE_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);
  return ret;
}

esp_err_t lsm303_config_default(){
  esp_err_t ret;
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  //ACC: CFG_1A
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, ( LSM303_SLAVE_ADDR_ACC << 1 ) | WRITE_BIT, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_CFG_1A, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_CFG_1A_ON, ACK_CHECK_EN);
  //ACC: CFG_4A
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, ( LSM303_SLAVE_ADDR_ACC << 1 ) | WRITE_BIT, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_CFG_4A, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_CFG_4A_FSHR, ACK_CHECK_EN);
  //MAG: CFG_MA
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, ( LSM303_SLAVE_ADDR_MAG << 1) | WRITE_BIT, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_CFG_MA, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_CFG_MA_ON, ACK_CHECK_EN);
  //MAG: CFG_MB
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, ( LSM303_SLAVE_ADDR_MAG << 1) | WRITE_BIT, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_CFG_MB, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_CFG_MB_LOW, ACK_CHECK_EN);
  //MAG: CFG_MM
  i2c_master_start(cmd);
  i2c_master_write_byte(cmd, ( LSM303_SLAVE_ADDR_MAG << 1) | WRITE_BIT, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_CFG_MM, ACK_CHECK_EN);
  i2c_master_write_byte(cmd, LSM303_CFG_MM_CONT, ACK_CHECK_EN);
  i2c_master_stop(cmd);
  ret = i2c_master_cmd_begin(I2C_EXAMPLE_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);
  return ret;
}

void lsm303_read_task(void *arg){
  while(1){
    esp_err_t ret;
    ret = lsm303_config_default();
    if(i2c_write_check(ret, "CFG")) goto END_I2C;
    byte *acc_data = (byte *) calloc(LSM303_ACC_DATA_LEN, 1);
    byte *mag_data = (byte *) calloc(LSM303_MAG_DATA_LEN, 1);
    byte *tmp_data = (byte *) calloc(LSM303_TMP_DATA_LEN, 1);
    while(1){
      ret = lsm303_read_acc(acc_data);
      if(i2c_read_check(ret, "ACC")) break;
      
      ret = lsm303_read_mag(mag_data);
      if(i2c_read_check(ret, "MAG")) break;
      
      ret = lsm303_read_tmp(tmp_data);
      if(i2c_read_check(ret, "TMP")) break;

      int xl = acc_data[0];
      int xh = ((signed char *)acc_data)[1];
      int yl = acc_data[2];
      int yh = ((signed char *)acc_data)[3];
      int zl = acc_data[4];
      int zh = ((signed char *)acc_data)[5];
      float acc_x = 0.0008f * ((xh << 8) + xl);
      float acc_y = 0.0008f * ((yh << 8) + yl);
      float acc_z = 0.0008f * ((zh << 8) + zl);
      int il = mag_data[1];
      int ih = ((signed char *)mag_data)[0];
      int jl = mag_data[3];
      int jh = ((signed char *)mag_data)[2];
      int kl = mag_data[5];
      int kh = ((signed char *)mag_data)[4];
      float mag_x = ((ih << 8) + il) / 1100.0;
      float mag_y = ((jh << 8) + jl) / 1100.0;
      float mag_z = ((kh << 8) + kl) / 1100.0;
      int tl = (tmp_data[1] >> 4) & 0xF;
      int th = tmp_data[0];
      float temp = ((th << 4) + tl) / 4.0;
      printf("Acceleration (normalised):   (%06f, %06f, %06f)\n", acc_x, acc_y, acc_z);
      printf("Magnetic Field (normalised): (%06f, %06f, %06f)\n", mag_x, mag_y, mag_z);
      printf("Temperature (normalised):    %3.2f\n", temp);
      vTaskDelay(1000/portTICK_RATE_MS);
    }
    free(acc_data);
    free(mag_data);
    free(tmp_data);
  END_I2C:
    vTaskDelay(1000/portTICK_RATE_MS);
  }
}

void app_main()
{
  print_mux = xSemaphoreCreateMutex();
  i2c_example_master_init();
  printf("Init done\n");
  xTaskCreate(lsm303_read_task, "LSM303 read task", 2048, NULL, 1, NULL);
}

