# LSM303 I2C

* Pin assignment:
 
    * master:
        * GPIO18 is assigned as the data signal of i2c master port
        * GPIO19 is assigned as the clock signal of i2c master port
 
* Connection:
 
    * connect GPIO18 with SDA
    * connect GPIO19 with SCL
    * no need to add external pull-up resistors, driver will enable internal pull-up resistors.
 

